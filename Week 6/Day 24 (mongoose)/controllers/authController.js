const jwt = require("jsonwebtoken"); // import jsonwebtoken

class AuthController {
  async getToken(req, res) {
    try {
      // Get the req.user that has been created in the authRoutes
      // And create body variable
      const body = {
        id: req.user._id,
      };

      // Create jwt token with { user: { id: req.user._id } } value
      // And the key is process.env.JWT_SECRET
      const token = jwt.sign(         //ini buat assign
        {
          user: body,                 //ini payload di jwt io
        },                            //jgn simpan data disini kalo bisa ya di local storage atau DB
        process.env.JWT_SECRET, {
          algorithm : "HS384",
          expiresIn : "30d", // expired token
        }        //ini secret key (encode / kunci gemboknya)
      );

      // If success
      return res.status(200).json({
        message: "Success",
        token, //biar bisa langsung login setelah daftar 
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new AuthController();
