const express = require("express");
const app = express();
const HelloRouter = require("./Routes/helloRoutes")

app.use("/", HelloRouter)

app.listen(3000, () => console.log("server running on 3000!"))