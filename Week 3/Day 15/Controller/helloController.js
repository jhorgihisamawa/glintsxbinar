class HelloController {
  async get(req, res) {
    try {
      console.log("ini untuk terminal1");
      if (req.params.name === "Jhorgi") {
        res.send("Jhorgi Hisamawa");
      } else {
        res.send("Bukan saya");
      }
    } catch (error) {
      res.send(error);
    }
  }

  async post(req, res) {
    try {
      console.log("ini untuk terminal2");
      if (req.params.name === "Jhorgi") {
        res.send("Jhorgi Hisamawa");
      } else {
        res.send("Bukan saya");
      }
    } catch (error) {
      res.send(error);
    }
  }

  async put(req, res) {
    try {
      console.log("ini untuk terminal3");
      if (req.params.name === "Jhorgi") {
        res.send("Jhorgi Hisamawa");
      } else {
        res.send("Bukan saya");
      }
    } catch (error) {
      res.send(error);
    }
  }

  async delete(req, res) {
    try {
      console.log("ini untuk terminal4");
      if (req.params.name === "Jhorgi") {
        res.send("Jhorgi Hisamawa");
      } else {
        res.send("Bukan saya");
      }
    } catch (error) {
      res.send(error);
    }
  }
}

module.exports = new HelloController();
