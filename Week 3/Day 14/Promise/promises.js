// Import fs
const fs = require("fs");

// Make promise object
const readFile = (file, options) =>
  new Promise((success, failed) => {
    fs.readFile(file, options, (err, content) => {
      if (err) failed(err);
      return success(content);
    });
  });

readFile("./Data/file1.txt", "utf-8")
  .then((content1) => {
    console.log(content1);
    return readFile("./Data/file2.txt", "utf-8");
  })
  .then((content2) => {
    console.log(content2);
    return readFile("./Data/file3.txt", "utf-8")
  })
  .then((content3) => {
    console.log(content3);
    return readFile("./Data/file4.txt", "utf-8")
  })
  .then((content4) => {
    console.log(content4);
    return readFile("./Data/file5.txt", "utf-8")
  })
  .then((content5) => {
    console.log(content5);
    return readFile("./Data/file6.txt", "utf-8")
  })
  .then((content6) => {
    console.log(content6);
    return readFile("./Data/file7.txt", "utf-8")
  })
  .then((content7) => {
    console.log(content7);
    return readFile("./Data/file8.txt", "utf-8")
  })
  .then((content8) => {
    console.log(content8);
    return readFile("./Data/file9.txt", "utf-8")
  })
  .then((content9) => {
    console.log(content9);
    return readFile("./Data/file10.txt", "utf-8")
  })
.then((content10)=>{
    console.log(content10)
    })
  .catch((error) => console.error("salah bro!"))