//Import fs
const fs = require("fs");

// Make promise object
const readFile = (file, options) =>
  new Promise((success, failed) => {
    fs.readFile(file, options, (err, content) => {
      if (err) failed(err);
      return success(content);
    });
  });

const readAllFiles = async () => {
  try {
    let data = await Promise.all([
        readFile("./Data/file1.txt", "utf-8"),
        readFile("./Data/file2.txt", "utf-8"),
        readFile("./Data/file3.txt", "utf-8"),
        readFile("./Data/file4.txt", "utf-8"),
        readFile("./Data/file5.txt", "utf-8"),
        readFile("./Data/file6.txt", "utf-8"),
        readFile("./Data/file7.txt", "utf-8"),
        readFile("./Data/file8.txt", "utf-8"),
        readFile("./Data/file9.txt", "utf-8"),
        readFile("./Data/file10.txt", "utf-8")
    ]);

    console.log(data[0])
    console.log(data[1])
    console.log(data[2])
    console.log(data[3])
    console.log(data[4])
    console.log(data[5])
    console.log(data[6])
    console.log(data[7])
    console.log(data[8])
    console.log(data[9])
    
    ;
    // console.log(data) //isi file keluar tapi jadi fungsi
   
  } catch (e) {
    console.log("What the error!");
  }
};

readAllFiles();
