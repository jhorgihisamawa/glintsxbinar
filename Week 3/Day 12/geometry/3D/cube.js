const ThreeDimention = require("../threeDimention");

class Cube extends ThreeDimention {
  constructor(side) {
    super("Cube");
    this.side = side;
  }

  // Overloading method
  introduce(who) {
    super.introduce();
    console.log(`${who}, this is ${this.name} \n`);
  }

  // Overridding
  // calculateArea() {
  //   super.calculateArea();
  //   let area = this.side ** 2;

  //   console.log(`This area is ${area} cm2 \n`);
  // }

  calculateVolume() {
    super.calculateVolume();
    let volume = this.side ** 3;

    console.log(`the ${this.name} is ${volume} cm3 \n`);
  }
}

module.exports = Cube;
