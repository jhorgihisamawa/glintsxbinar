const { Square, Rectangle, Triangle } = require("./geometry");
const { Beam, Cone, Cube, Tube } = require("./geometry")

console.log("-------------Area------------------")

let trySquare = new Square(17);
trySquare.calculateArea();

let tryRectangle = new Rectangle(11, 12);
tryRectangle.calculateArea();

let tryTriangle = new Triangle(20,35)
tryTriangle.calculateArea()

console.log("-----------------------Volume-----------------\n")

let tryBeam = new Beam(17,23,45);
tryBeam.calculateVolume();

let tryCone = new Cone(15, 12);
tryCone.calculateVolume();

let tryCube = new Cube(20)
tryCube.calculateVolume()

let tryTube = new Tube(34,21)
tryTube.calculateVolume()