const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function beam(length,width,height) {
    return length*width*height
}

    function inputlength() {
        rl.question(`length: `, (length) => {
        if(!isNaN(length)) {
            inputwidth(length)
        } else {
            console.log(`length must be a number\n`)
            inputlength()
        }
    })
}


function inputwidth(length) {
    rl.question(`width: `, (width) => {
    if(!isNaN(width)) {
        inputheight(length,width)
    } else {
        console.log(`width must be a number/n`)
        inputwidth(length)
    }
})
}

function inputheight(length,width) {
    rl.question(`height: `, (height) => {
    if(!isNaN(height)) {
        console.log(`\nBeam Volume: ${beam(length, width, height)}`)
        rl.close()
    } else {
        console.log(`height must be a number/n`)
        inputheight(length,width)
    }
})
}

console.log("Beam Volume")
console.log("============")

inputlength()