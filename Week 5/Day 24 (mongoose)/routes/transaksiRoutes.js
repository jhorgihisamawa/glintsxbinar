const express = require("express");

// Import validator
const transaksiValidator = require("../middlewares/validators/transaksiController");
// Import controller
const transaksiController = require("../controllers/transaksiController");


// Make router
const router = express.Router();

// Get all transaksi data
router.get("/", transaksiController.getAll);
router.get("/:id", transaksiController.getOne);
router.post("/", transaksiController.create);

module.exports = router;
