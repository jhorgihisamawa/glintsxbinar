const mongoose = require("mongoose") // Import monggoose

const uri = process.env.MONGO_URI //Add URI MongoDB Atlas

// Connect express to MongoDB with mongose
mongoose.connect(uri, {
    useUnifiedTopology: true, // Must be added
    useNewUrlParser: true, // Must be added
    useCreateIndex: true, // Use to enable unique data type
    useFindAndModify: false, // Use findOneAndUpdate instead of findAndModify
});

//Import models

const transaksi = require("./transaksi");
const pelanggan = require("./pelanggan");
const pemasok = require("./pemasok");
const barang = require("./barang");

module.exports = { transaksi, barang, pemasok, pelanggan }