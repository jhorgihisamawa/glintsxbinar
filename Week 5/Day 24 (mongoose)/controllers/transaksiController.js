const { barang, pelanggan , pemasok, transaksi } = require("../models");

class TransaksiController {
  // Get All
  async getAll(req, res) {
    try {
      // Find all data
      let data = await transaksi.find();

      // If no data
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOne(req, res) {
    try {
      // Find one data
      let data = await transaksi.findOne({
        _id: req.params.id,
      });

      //if data not found
      if (!data) {
        return res.status(404).json({
          message: "Transaksi Not FOund",
        });
      }
      // If success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async create(req, res) {
    try {
      let findData = await Promise.all([
        barang.findOne({ _id: req.body.id_barang }),
        pelanggan.findOne({ _id: req.body.id_pelanggan }),
      ]);

      //If barang/pelanggan not found
      if (!findData[0] || !findData[1]) {
        return res.status(500).json({
          message: "Barang or Transaksi not found",
        });
      }
      let total = eval(findData[0].harga * req.body.jumlah);

      let data = await transaksi.create({
        barang: findData[0],
        pelanggan: findData[1],
        jumlah: req.body.jumlah,
        total,
      });
      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      console.log(e)
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new TransaksiController();
