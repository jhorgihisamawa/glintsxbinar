const express = require("express"); //import express
const app = express(); //create import app
const transaksiRoutes = require("./Routers/transaksiRoutes");

// Use to read the req.body
app.use(express.urlencoded({ extended: true }));
// kalo di postman di body raw makanya true
//gak akan muncul di url

// Make route
app.use("/transaksi", transaksiRoutes);

// Server run on port 3000
app.listen(3000, () => console.log("This server running on 3000"));
