const express = require("express"); //import express
const router = express.Router(); //make a router

//import controller
const transaksiController = require("../Controllers/transaksiController");

//defines routes
// If user go to http://localhost:3000/transaksi (GET)
router.get("/", transaksiController.getAll);
router.post("/", transaksiController.create);
router.get("/:id", transaksiController.getOne);
router.put("/:id", transaksiController.update);
router.delete("/:id", transaksiController.deleteData);

module.exports = router; //export router
